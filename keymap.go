/*
 * Copyright 2019 Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

// Variant struct
type Variant struct {
	code, parent, description string
}

// Layout struct
type Layout struct {
	code, description string
	variants          []Variant
}

// Handle error return values in a separate function to make code easier to read
func handle(err error, message string) error {
	if err != nil {
		log.Fatalf("%s: %v", message, err)
		return fmt.Errorf("%s: %v", message, err)
	}
	return nil
}

// Open configuration file, validate its content and return it as a string
func readConfig(conf string) (string, error) {
	// Opening file
	f, err := ioutil.ReadFile(conf)
	handle(err, "can't open keyboard config file")

	// Checking if the file contains valid layouts
	if strings.Contains(string(f), "! layout") {
		if strings.Contains(string(f), "! variant") {
			// Return file content if it is valid
			return string(f), nil
		}
	}
	// Return error otherwise
	return string(f), fmt.Errorf("Invalid layouts files")
}

// Select and extract the config section
func extract(s []string, identifier string) []string {
	var lines []string
	for _, section := range s {
		if strings.HasPrefix(section, identifier) {
			lines = strings.Split(section, "\n")
			// Remove first header line and last empty lines
			lines = lines[1 : len(lines)-2]
		}
	}
	return lines
}

// Transform the input text to make it cleaner and easier to parse
func transform(s string) string {
	// Remove leading and trailing spaces
	s = strings.TrimSpace(s)
	// Replace first space with | separator
	s = strings.Replace(s, " ", "|", 1)
	// Replace ": " with | separator
	s = strings.Replace(s, ": ", "|", 1)
	// Remove all the double spaces
	s = strings.Replace(s, "  ", "", -1)
	return s
}

// Parse a string and return a layout
func parseLayout(s string) Layout {
	tmp := strings.Split(s, "|")
	var l Layout
	l.code = tmp[0]
	l.description = tmp[1]
	return l
}

// Parse a string and return a variant
func parseVariant(s string) Variant {
	tmp := strings.Split(s, "|")
	var v Variant
	v.code = tmp[0]
	v.parent = tmp[1]
	v.description = tmp[2]
	return v
}

// GetKeymaps Parse the config file and build a keymap structure from it
func GetKeymaps(conf string) []Layout {
	// Open config file
	s, err := readConfig(conf)
	handle(err, "can't open keymap file")

	// Split config into sections
	sections := strings.Split(s, "! ")

	// Extract Layouts
	laystr := extract(sections, "layout")

	// Extract Variants
	varstr := extract(sections, "variant")

	// Transform layouts
	for i, ii := range laystr {
		laystr[i] = transform(ii)
	}

	// Transform variants
	for i, ii := range varstr {
		varstr[i] = transform(ii)
	}

	// Create final structures
	layouts := make([]Layout, len(laystr))
	variants := make([]Variant, len(varstr))

	// Parse layouts
	for l, ll := range laystr {
		layouts[l] = parseLayout(ll)
	}

	// Parse variants
	for v, vv := range varstr {
		variants[v] = parseVariant(vv)
	}

	// Append variants to corresponding layouts
	for m, mm := range layouts {
		for _, nn := range variants {
			if nn.parent == mm.code {
				layouts[m].variants = append(layouts[m].variants, nn)
			}
		}
	}
	return layouts
}
