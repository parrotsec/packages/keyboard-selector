# Parrot Keyboard Selector
This repository contains the source code of our keyboard selector. Feel free to make your contribution

## Getting Started

### Prerequisites

*  [go](https://golang.org/doc/install)

### Build and run

`$ make`

## Contributing

Please read [CONTRIBUTING.md].md](CONTRIBUTING.md) for details, and the process for submitting pull requests to us.

## Developers

* **Pino "Mastrobirraio" Matranga** - _mastrobirraio@parrotsec.org_
* **Lorenzo "Palinuro" Faletra** - _palinuro@mastrobirraio.org_

## License
This project is licensed under the GPLv3 and CC-BY-SA v4.0 Licenses - see the [LICENSE.md].md](LICENSE.md) file for details.
