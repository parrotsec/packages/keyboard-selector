/*
 * Copyright 2019 Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package main

import (
	"log"
	"net"
	"net/http"

	"github.com/zserge/webview"
)

const (
	windowWidth  = 400
	windowHeight = 600
)

var head = `
<html lang="en">
    <head>
        <title></title>
		<link rel="stylesheet" type="text/css" href="src/css/main.css">
		<style>
			body {
				margin: 0;
				background-color: #4A4A4A;
			}
			table.darkTable {
				border-spacing: 2px;
				border: 2px;
				background-color: #4A4A4A;
				width: 100%;
				height: 200px;
				text-align: center;
				border-collapse: collapse;
			}
			table.darkTable tr {
				display: table-row;
				vertical-align: inherit;
				border-color: inherit;
			}
			table.darkTable tr:hover {
				background-color: grey;
				cursor: pointer;
			}
			table.darkTable thead {
				background: #000000;
				display: table-header-group;
				vertical-align: middle;
			}
			table.darkTable td, table.darkTable th {
				border: 1px;
				padding-top: 16px;
				padding-bottom: 16px;
			}
			table.darkTable tbody td {
				font-size: 15px;
				line-height: 1.4;
				color: #E6E6E6;
			}
			table.darkTable thead th {
				font-size: 15px;
				font-weight: bold;
				color: #E6E6E6;
				text-align: center;
			}
			table.darkTable thead th:first-child {
				border-left: none;
			}
		</style>
    </head>
    <body>
    <table class="darkTable">
        <thead>
            <tr>
                <th>code</th>
                <th>lang</th>
            </tr>
        </thead>
		<tbody>
`

var tail = `
        </tbody>
    </table>
    </body>
</html>
`

func startServer(html string) string {
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		defer ln.Close()
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(html))
		})
		log.Fatal(http.Serve(ln, nil))
	}()
	return "http://" + ln.Addr().String()
}

func handleRPC(w webview.WebView, data string) {
	switch {
	case data == "close":
		w.Terminate()
	case data == "message":
		w.Dialog(webview.DialogTypeAlert, 0, "Hello", "Hello, world!")
	case data == "info":
		w.Dialog(webview.DialogTypeAlert, webview.DialogFlagInfo, "Hello", "Hello, info!")
	case data == "warning":
		w.Dialog(webview.DialogTypeAlert, webview.DialogFlagWarning, "Hello", "Hello, warning!")
	case data == "error":
		w.Dialog(webview.DialogTypeAlert, webview.DialogFlagError, "Hello", "Hello, error!")
	}
}

func main() {
	layouts := GetKeymaps("/usr/share/X11/xkb/rules/xorg.lst")

	var list string
	for _, l := range layouts {
		list += "<tr><td>" + l.code + "</td><td>" + l.description + "</td></tr>"
	}

	html := head + list + tail
	url := startServer(html)
	w := webview.New(webview.Settings{
		Width:                  windowWidth,
		Height:                 windowHeight,
		Title:                  "Select Keyboard Layout",
		Resizable:              false,
		URL:                    url,
		ExternalInvokeCallback: handleRPC,
	})
	defer w.Exit()
	w.Run()
}
